// Generate labeled tape.

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

typedef uint64_t word36;

#define MASK36  0777777777777lu
#define BIT37  01000000000000lu  // carry out bit from 36 bit arithmetic

// From mstr.incl.pl1

struct hdr
  {
    /* 0 */ word36 c1;
    /* 1 */ word36 uid[2];
    /* 3 */ word36 rec_fil; // word18 rec_within_file, phy_file
    /* 4 */ word36 data_bits; // word18 data_bits_used, data_bits_len
    /* 5 */ word36 flags; // word24 bits; word3 head_version; word9 repeat_count
    /* 6 */ word36 checksum;
    /* 7 */ word36 c2;
  };

struct trlr
  {
    /* 0 */ word36 c1;
    /* 1 */ word36 uid[2];
    /* 3 */ word36 tot_data_bits;
    /* 4 */ word36 pad_pattern;
    /* 5 */ word36 reel_file; // word12 reel_num; word24 tot_file;
    /* 6 */ word36 tot_rec;
    /* 7 */ word36 c2;
  };

struct lbl
  {
    struct hdr hdr;
    word36 data[1024];
    struct trlr trlr;
  };

static struct lbl label;

// The packed label

#define blen (((1024 + 8 + 8) * 9) / 2)
static uint8_t bits[blen];

// Write a 36 bit word into a packed buffer

static void put36 (word36 val, uint8_t * bits, uint woffset)
  {
    uint isOdd = woffset % 2;
    uint dwoffset = woffset / 2;
    uint8_t * p = bits + dwoffset * 9;

    if (isOdd)
      {
        p [4] &=               0xf0;
        p [4] |= (val >> 32) & 0x0f;
        p [5]  = (val >> 24) & 0xff;
        p [6]  = (val >> 16) & 0xff;
        p [7]  = (val >>  8) & 0xff;
        p [8]  = (val >>  0) & 0xff;
      }
    else
      {
        p [0]  = (val >> 28) & 0xff;
        p [1]  = (val >> 20) & 0xff;
        p [2]  = (val >> 12) & 0xff;
        p [3]  = (val >>  4) & 0xff;
        p [4] &=               0x0f;
        p [4] |= (val <<  4) & 0xf0;
      }
  }

// Read the SCU clock

static uint64_t clk (void) // SCU clock is 52 bits
  {
    struct timeval now;
    gettimeofday(& now, NULL);
    uint64_t UNIX_secs = (uint64_t) now.tv_sec;
    uint64_t UNIX_usecs = UNIX_secs * 1000000ul + (uint64_t) now.tv_usec;

    // now determine uSecs since Jan 1, 1901 ...
    uint64_t Multics_usecs = 2177452800000000ul + UNIX_usecs;
    return Multics_usecs;
  }

static void usage (void)
  {
    fprintf (stderr, "Usage: label <installation> <reel_id>\n"
                     "32 char. max.\n");
    exit (1);
  }

// String copy from char * to 32 9-bit bytes; blank padded.

void str9cpy (word36 * dest, char * src)
  {
    size_t l = strlen (src);
    word36 w = 0;
    for (int i = 0; i < 32; i ++)
      {
        w <<= 9;
        if (i < l)
          w |= (unsigned char) src[i];
        else
          w |= (unsigned char) ' ';
        if (i % 4 == 3)
          {
            * dest ++ = w;
            w = 0;
          }
      }
  }

// Checksum computation

// tape_checksum_:
//          eppbp     ap|2,*              bp -> physical record pointer
//          eppbp     bp|0,*              bp -> physical record
//
//          eax1      0                   x1 is physical record header index
//          ldq       bp|4                get length of data in bits
//          anq       =o777777,dl         isolate record size
//          div       36,dl               compute word count
//          eax2      8,ql                x2 is physical record trailer index
//          eax3      1                   x3 is rotate index
//          lda       0,dl                clear the a register
//          ldi       =o4000,dl           clear indicators and set overflow mask
//
//odd;      rpda      6,1                 do the record header
//          awca      bp|0,1              compute checksum on header
//          alr       0,3                 ..
//          awca      bp|1,1              add in last word of header
//          alr       0,3                 ..
//
//odd;      rpda      8,1                 now do the trailer
//          awca      bp|0,2              compute checksum on trailer
//          alr       0,3                 ..
//
//          awca      0,dl                add in any remaining carries
//
//          eppbp     ap|4,*              bp -> checksum pointer
//          sta       bp|0,*              store the checksum

static uint64_t csum, carry;

static void init_cksum (void)
  {
    csum = carry = 0;
  }

// Add With Carry

static void awca (word36 w)
  {
    // A = A + operand + carry
    csum += w + carry;
    // Extract carry out bit
    carry = (csum & BIT37) ? 1 : 0;
    // Zero carry out bit
    csum &= MASK36;
  }

// Left rotate

static void alr (void)
  {
    // Shift left
    csum << 1;
    // Get shifted high bit
    word36 bit = (csum & BIT37) ? 1 : 0;
    // Clear shifted high bit and low bit
    csum &= 0777777777776lu;
    // Insert shifted high bit into low bit
    csum |= bit;
  }

static void cksum (word36 w)
  {
    awca (w);
    alr ();
  }

static word36 compute_csum (void)
  {
    init_cksum ();
    cksum (label.hdr.c1);
    cksum (label.hdr.uid[0]);
    cksum (label.hdr.uid[1]);
    cksum (label.hdr.rec_fil);
    cksum (label.hdr.data_bits);
    cksum (label.hdr.flags);
    cksum (label.hdr.c2);
    cksum (label.trlr.c1);
    cksum (label.trlr.uid[0]);
    cksum (label.trlr.uid[1]);
    cksum (label.trlr.tot_data_bits);
    cksum (label.trlr.pad_pattern);
    cksum (label.trlr.reel_file);
    cksum (label.trlr.tot_rec);
    cksum (label.trlr.c2);
    awca (0l);
    return csum;
  }

// AG91 MPM Ref. Guide, App. F

int main (int argc, char * argv[])
  {
//#define TEST_CHKSUM
#ifdef TEST_CHKSUM
// 0000:0000   670314355245   000001653244   "\670\314\355\245\000\001\653\244
// 0000:0002   744031321540   000000000000   "\744\031\321\540\000\000\000\000
// 0000:0004   001540110000   600012001000   "\001\540H\000\600\012\001\000
// 0000:0006   771524146554   512556146073   "\771\524f\554\512\556f;
// 
// 0000:2010   107463422532   000001653244   "G\463\422\532\000\001\653\244
// 0000:2012   744031321540   000000001540   "\744\031\321\540\000\000\001\540
// 0000:2014   777777777777   000000000000   "\777\777\777\777\000\000\000\000
// 0000:2016   000000000000   265221631704   "\000\000\000\000\265\221\631\704
    init_cksum ();
    cksum (0670314355245ul);
    cksum (0000001653244ul);
    cksum (0744031321540ul);
    cksum (0000000000000ul);
    cksum (0001540110000ul);
    cksum (0600012001000ul);
//771524146554
    cksum (0512556146073ul);

    cksum (0107463422532ul);
    cksum (0000001653244ul);
    cksum (0744031321540ul);
    cksum (0000000001540ul);
    cksum (0777777777777ul);
    cksum (0000000000000ul);
    cksum (0000000000000ul);
    cksum (0265221631704ul);
    awca (0ul);
    printf ("got %012lu; expected 771524146554\n", csum);
    exit (1);
#endif

// Verify arguments

    if (argc != 3)
      usage ();
    size_t s1 = strlen (argv [1]);
    if (s1 > 32)
      usage ();
    size_t s2 = strlen (argv [2]);
    if (s2 < 1 || s2 > 32)
      usage ();

/// Build the image file name

    char fname[37]; // 32 + ".tap\0"
    strcpy (fname, argv[2]);
    strcat (fname, ".tap");

// Check for exisiting file

    struct stat stat_buf;
    int rc = stat (fname, & stat_buf);
    if (rc == 0)
      {
        fprintf (stderr, "Error -- image file exists\n");
        usage ();
      }

// Create the file

    int fd = creat (fname, 0644);
    if (fd < 0)
      {
        perror ("creat");
        usage ();
      }

// Generate the UUID

    uint64_t now = clk ();
    //printf ("%024lo\n", now);

// Initialize the header

    label.hdr.c1 = 0670314355245lu;
    label.hdr.uid[0] = (now >> 36) & MASK36;
    label.hdr.uid[1] = (now >>  0) & 0177777lu;
    label.hdr.rec_fil = 0;
    // data_bits_used = 001540 = 864 bits = 24 words
    // data_bits_len = 0110000 = 36864 bits = 1024 words
    label.hdr.data_bits = 0001540110000lu;
    // 6...........
    //   admin 1
    //   label 1
    //   eor 0
    // .0001.......
    // pad1 000 000 000 00
    // ....1.......
    // set 1
    // .....2......
    // repeat 0
    // padded 1
    // eot 0
    // ......00....
    // drain 0
    // continue 0
    // pad2 0000
    // ........1...
    // header_version 1
    // .........000
    // repeat_count 0
    label.hdr.flags = 0600012001000lu;
    //label.hdr.checksum = 0201373135777lu;
    label.hdr.checksum = 0;
    label.hdr.c2 = 0512556146073lu;

// Set the site and reel ID

    str9cpy (& label.data[0], argv[1]);
    str9cpy (& label.data[8], argv[2]);

// Initialize the remainder of the data block

    for (int i = 16; i < 1024; i ++)
      label.data[i] = MASK36;

// Set up the trailer

    label.trlr.c1 = 0107463422532lu;
    label.trlr.uid[0] = (now >> 36) & MASK36;
    label.trlr.uid[1] = (now >>  0) & 0177777lu;
    label.trlr.tot_data_bits = 0000000001540lu;
    label.trlr.pad_pattern = MASK36;
    label.trlr.reel_file = 0000000000000lu;
    label.trlr.tot_rec = 0000000000000lu;
    label.trlr.c2 = 0265221631704lu;

// Compute the checksum

    label.hdr.checksum = compute_csum ();

// Convert to packed

    word36 * p = (word36 *) & label;
    for (int i = 0; i < 1024 + 8 + 8; i ++)
       put36 (p[i], bits, i);

// Write the SIMH block size

    int32_t rec_len = blen;
    write (fd, & rec_len, sizeof (rec_len));

// Write the label

    write (fd, bits, sizeof (bits));

// Write the SIMH block size

    write (fd, & rec_len, sizeof (rec_len));

// Write two tape marks

    int32_t tape_mark = 0;
    write (fd, & tape_mark, sizeof (tape_mark));
    write (fd, & tape_mark, sizeof (tape_mark));

// Write end-of-medium

    int32_t eom = -1;
    write (fd, & eom, sizeof (eom));

// Cleanup

    close (fd);

    return 0;
  }

